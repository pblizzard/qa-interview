package com.bibliocommons;

import java.util.Properties;
import java.io.IOException;
import java.io.InputStream;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

public class TestProperties {
	private transient String propertiesFile = "/webdriver.properties";
	private transient Properties wdProperties = new Properties();
	
	private String host;
	private String port;
	
	private String os;
	private String browser;
	private String version;
	
	private static Logger logger;
	
	public TestProperties() {
		logger = Logger.getLogger(getClass().getName());

		PropertyConfigurator.configure("resources/log4j.properties");

		InputStream is = TestProperties.class.getResourceAsStream(propertiesFile);

		try {
			wdProperties.load(is);
			
			host = wdProperties.getProperty("host");
			port = wdProperties.getProperty("port");
			
			String[] browserParts = wdProperties.getProperty("browser").split(";");
			
			os = browserParts[0];
			browser = browserParts[1];
			version = browserParts[2];
			
			
		} catch (IOException e)
		{
			logger.error("Error opening webdriver.properties...");
			logger.error(e);
			logger.error(e.getMessage());
		}
		
	}

	/**
	 * @return host target for testing
	 */
	public String getHost() {
		return host;
	}

	/**
	 * @return port target for testing
	 */
	public String getPort() {
		return port;
	}

	/**
	 * @return os provisioned for testing
	 */
	public String getOs() {
		return os;
	}

	/**
	 * @return browser provisioned for testing
	 */
	public String getBrowser() {
		return browser;
	}

	/**
	 * @return browser version provisioned for testing
	 */
	public String getVersion() {
		return version;
	}
	
	
	
	
}
