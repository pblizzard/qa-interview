package com.bibliocommons.pageobject;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;

public abstract class TestPage {
	
	public int DRIVER_WAIT = 10;
	protected WebDriver wd;
	protected static Logger logger;
	protected String host;
	
	public TestPage(WebDriver wd, String dn)
	{
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(wd, DRIVER_WAIT);
		PageFactory.initElements(finder, this);
		this.wd = wd;

		logger = Logger.getLogger(getClass().getName());
		PropertyConfigurator.configure("resources/log4j.properties");

		host = dn;
	}
}
