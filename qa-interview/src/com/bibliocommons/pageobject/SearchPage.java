package com.bibliocommons.pageobject;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

public class SearchPage extends TestPage {

	@FindBy(css = "select[testid='select_facet']")
	private WebElement searchType;

	@FindBy(css = "input[testid='button_search']")
	private WebElement searchButton;

	@FindBy(css = "input[testid='field_search']")
	private WebElement searchQuery;
	
	@FindBy(css = "ul[class*='ui-autocomplete']")
	private WebElement autocompleteMenu;

	@FindBy(css = "ul[class*='ui-autocomplete'] li[class='ui-menu-item'][role='menuitem']")
	private List<WebElement> autoCompleteItems;

	
	public SearchPage(WebDriver wd, String dn) {
		super(wd, dn);
	}
	
	public SearchResultsPage search(QueryType qt, String query)
	{

		Select searchSelect = new Select(searchType);
		
		searchSelect.selectByVisibleText(qt.name());
		searchQuery.clear();
		searchQuery.sendKeys(query);
		searchButton.click();
		
		return new SearchResultsPage(wd, host);
	}
	
	public enum QueryType
	{
		Keyword,
		Title,
		Subject,
		Author,
		Tag,
		List,
		User
	}

}
