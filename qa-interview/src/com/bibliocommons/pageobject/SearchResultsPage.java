package com.bibliocommons.pageobject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

public class SearchResultsPage extends TestPage {

	@FindBy(css = "div#bibList")
	private WebElement bibList;
	
	@FindBy(css = "div#viewSelector")
	private WebElement viewOption;
	
	@FindBy(css = "select#sortSelector")
	private WebElement sortSelector;
	
	@FindBy(css = "select[testid='select_page']")
	private WebElement pageSelect;
	
	@FindBy(css = "p.total")
	private WebElement totalCount;
	
	@FindBy(css = "a[testid='link_nextpage']")
	private WebElement nextPage;

	@FindBy(css = "a[testid='link_prevpage']")
	private WebElement prevPage;
	
	public SearchResultsPage(WebDriver wd, String dn) {
		super(wd, dn);
	}
	
	//Page Actions
	
	public SearchResultsPage coverView()
	{
		viewOption.findElement(By.cssSelector("a[testid='link_viewmedium']")).click();
		return new SearchResultsPage(wd, host);
	}

	public SearchResultsPage listView()
	{
		viewOption.findElement(By.cssSelector("a[testid='link_viewsmall']")).click();
		return new SearchResultsPage(wd, host);
	}
	
	public SearchResultsPage sort(String by)
	{
		new Select(sortSelector).selectByVisibleText("Sort by " + by);

		return new SearchResultsPage(wd, host);
	}
	
	public SearchResultsPage selectPage(int page)
	{
		new Select(pageSelect).selectByVisibleText(Integer.toString(page));
		return new SearchResultsPage(wd,host);
	}
	
	public SearchResultsPage nextPage()
	{
		nextPage.click();
		return new SearchResultsPage(wd,host);
	}

	public SearchResultsPage prevPage()
	{
		prevPage.click();
		return new SearchResultsPage(wd,host);
	}
	
	//Page Assertions
	
	public int getResultsCount()
	{
		return Integer.parseInt(totalCount.findElement(By.cssSelector("strong")).getText());
	}
	
	public boolean isBibListPresent()
	{
		return bibList.isDisplayed();
	}

}
