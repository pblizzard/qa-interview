package com.bibliocommons;

import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.DesiredCapabilities;

public abstract class TestBase {
	
	protected static Logger logger;
	protected WebDriver wd;
	private DesiredCapabilities dc;
	private TestProperties tp;
	
	protected String fqdn;
	
	public TestBase() {
		logger = Logger.getLogger(getClass().getName());
		PropertyConfigurator.configure("resources/log4j.properties");
		
		dc = new DesiredCapabilities();
		tp = new TestProperties();
		
	}
	
	@Before
	public void setUp() {
		
		fqdn = "http://"+tp.getHost()+":"+tp.getPort();
		
		logger.info("Setting up WebDriver...");
		
		// Default firefox init
		FirefoxProfile firefoxProf = new FirefoxProfile();

		// don't check for ff updates
		firefoxProf.setPreference("app.update.enabled", false);
	
		dc.setCapability("firefox_profile", firefoxProf);
		
		// create local WebDriver
		if (tp.getBrowser().contains("firefox")) wd = new FirefoxDriver(dc);
		else if (tp.getBrowser().contains("iexplore")) logger.error("Not implemented");
		else if (tp.getBrowser().contains("googlechrome")) logger.error("Not implemented");
		else logger.error("Must select a browser (see webdriver.properties)");
		
		//set implicit timeout
		wd.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		logger.info("Beginning test...");

	}
	
	@After
	public void tearDown() {
		// done!
		logger.info("Ending test...");
		wd.quit();
	}

	/**
	 * @return active WebDriver
	 */
	public WebDriver getWebDriver() {
		return wd;
	}	
}
