package com.bibliocommons;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.bibliocommons.TestBase;
import com.bibliocommons.pageobject.SearchPage;
import com.bibliocommons.pageobject.SearchPage.QueryType;
import com.bibliocommons.pageobject.SearchResultsPage;


public class TestInterview extends TestBase {
	
	@Test
	public void keywordSearch () {
		wd.get(fqdn+"/");
		SearchPage sp = new SearchPage(wd,fqdn);
		SearchResultsPage srp = sp.search(QueryType.Keyword, "harry potter");
		
		assertTrue(srp.isBibListPresent());
		assertTrue(srp.getResultsCount() > 0);
		
		
	}
	
	
	
}
